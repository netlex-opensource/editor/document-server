# Netlex Editor
Este repositório contém o código do editor utilizado no netlex.
Este código é um fork do [Document Server](https://github.com/ONLYOFFICE/DocumentServer) criado pelo onlyoffice.

## Clone

Este código contém submódulos necessários para sua execução, portanto deve ser clonado de forma a incluir estes submódulos.

### ssh
```
$ git clone --recurse-submodules git@gitlab.com:netlex/netlex-document-server.git
```
### https
```
$ git clone --recurse-submodules https://gitlab.com/netlex/netlex-document-server.git
```


## Build

O build do editor é realizado utilizando um Dockerfile com a estratégia de multistage.

#### netlex-editor-core

O primeiro estágio é imagem netlex-editor-core. O build desta imagem é demasiadamente demorado, para que não seja necessário realizar este build com frequência esta imagem pode ser baixada do repositório.

O download pode ser realizado da seuinte forma:

###### login
```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 656835574920.dkr.ecr.us-east-1.amazonaws.com
```

###### pull
```
docker pull 656835574920.dkr.ecr.us-east-1.amazonaws.com/netlex-editor-core:latest
```

É necessário aplicar a tag na imagem para que esta possa ser reconhecida pelos estágios de build seguintes

```
docker tag 656835574920.dkr.ecr.us-east-1.amazonaws.com/netlex-editor-core:latest netlex-editor-core:latest 
```

###### build 
Caso a imagem armazenada não atenda e seja necessário criar um novo build da imagem

```
$ docker build -t netlex-editor-core:latest -f Dockerfile.core .
```

#### netlex-editor (development)

Para criar o build direcionado para o ambiente de desenvolvimento

```
$ docker build --target development -t netlex-editor-dev:latest .
```


#### netlex-editor (production)

Para criar o build direcionado para o ambiente de produção

```
$ docker build --target production -t netlex-editor:latest .
```

## Teste
Execute o arquivo test.html no seu browser.