############[STAGE 2]###############
FROM ubuntu:18.04 as build

ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y sudo curl

RUN curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

RUN apt-get update && apt-get install -y \
    git \
    nodejs \
    closure-compiler \
    build-essential 

RUN npm install -g npm grunt-cli grunt pkg

#
#---[MAIN FOLDER]
#
RUN adduser --quiet --system --group --home /onlyoffice onlyoffice && \
    echo "onlyoffice ALL=(ALL) NOPASSWD:ALL" | tee -a /etc/sudoers

#
#---[CORE]
#
COPY --from=netlex-editor-core:latest /onlyoffice/DocumentServer/core /onlyoffice/DocumentServer/core

#
#---[SERVER]
#
COPY ./server /onlyoffice/DocumentServer/server

WORKDIR /onlyoffice/DocumentServer/server
RUN git init . && npm install && grunt --no-color -v

#-doc-service
WORKDIR /onlyoffice/DocumentServer/server/build/server/DocService
RUN pkg . -t node10-linux --options max_old_space_size=4096 -o docservice

#-file-converter
WORKDIR /onlyoffice/DocumentServer/server/build/server/FileConverter
RUN mkdir bin && cp /onlyoffice/DocumentServer/core/build/bin/linux_64/x2t ./bin/
RUN pkg . -t node10-linux -o converter

#-metrics
WORKDIR /onlyoffice/DocumentServer/server/build/server/Metrics
RUN pkg . -t node10-linux -o metrics

#-spell-checker
WORKDIR /onlyoffice/DocumentServer/server/build/server/SpellChecker
RUN pkg . -t node10-linux -o spellchecker

#
#---[JS]
#
COPY ./core-fonts /onlyoffice/DocumentServer/core-fonts
COPY ./sdkjs /onlyoffice/DocumentServer/sdkjs
COPY ./web-apps /onlyoffice/DocumentServer/web-apps
RUN cp /onlyoffice/DocumentServer/core/Common/3dParty/icu/linux_64/build/*.58 /onlyoffice/DocumentServer/core/build/lib/linux_64/

#-fonts
ENV LD_LIBRARY_PATH=/onlyoffice/DocumentServer/core/build/lib/linux_64

RUN mkdir -p /onlyoffice/DocumentServer/fonts
RUN mkdir -p /onlyoffice/DocumentServer/font-files/allfonts
RUN mkdir -p /onlyoffice/DocumentServer/font-files/allfonts-web
RUN mkdir -p /onlyoffice/DocumentServer/font-files/selection

RUN /onlyoffice/DocumentServer/core/build/bin/linux_64/allfontsgen \
    --input=/onlyoffice/DocumentServer/core-fonts \
    --images=/onlyoffice/DocumentServer/sdkjs/common/Images \
    --selection=/onlyoffice/DocumentServer/font-files/selection/font_selection.bin \
    --allfonts=/onlyoffice/DocumentServer/font-files/allfonts/AllFonts.js \
    --allfonts-web=/onlyoffice/DocumentServer/font-files/allfonts-web/AllFonts.js \
    --output-web=/onlyoffice/DocumentServer/fonts \
    --use-system=true

#-sdkjs
RUN cd /onlyoffice/DocumentServer/sdkjs/build && npm install && grunt

#-web-apps
RUN cd /onlyoffice/DocumentServer/web-apps/build && npm install && grunt
RUN cd /onlyoffice/DocumentServer/web-apps/build/sprites && npm install && grunt
####################################


############[STAGE 3]###############
FROM ubuntu:18.04 as common
ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#---[DEPENDENCIES]---#
RUN apt-get update && apt-get install -y sudo curl

RUN curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

RUN apt-get update && apt-get install -y \
    nginx \
    nodejs \
    postgresql \
    rabbitmq-server
#--------------------#

#---[NGINX-CONFIG]---#
COPY ./onlyoffice-documentserver /etc/nginx/sites-available/onlyoffice-documentserver
COPY ./nginx.conf /etc/nginx/nginx.conf

RUN rm -f /etc/nginx/sites-enabled/default && \
    ln -s /etc/nginx/sites-available/onlyoffice-documentserver /etc/nginx/sites-enabled/onlyoffice-documentserver
#--------------------#

#----[POSTGREE-SQL-CONFIG]----#
COPY ./postgresql.conf /etc/postgresql/12/main/
COPY ./server/schema/postgresql/createdb.sql ./createdb.sql

RUN service postgresql start && \
    sudo -i -u postgres psql -c "CREATE DATABASE onlyoffice;" && \
    sudo -i -u postgres psql -c "CREATE USER onlyoffice WITH password 'onlyoffice';" && \
    sudo -i -u postgres psql -c "GRANT ALL privileges ON DATABASE onlyoffice TO onlyoffice;" && \
    PGPASSWORD=onlyoffice psql -hlocalhost -Uonlyoffice -d onlyoffice -f createdb.sql && \
    rm createdb.sql
#----------------------------#

#----[RABBITMQ-CONFIG]----#
RUN sed -i "s/^#NODE_IP_ADDRESS=127.0.0.1.*/NODE_IP_ADDRESS=127.0.0.1/g" /etc/rabbitmq/rabbitmq-env.conf
#----------------------------#

RUN mkdir -p /var/www/onlyoffice/documentserver \
    /var/lib/onlyoffice/documentserver/App_Data/cache/files

WORKDIR /var/www/onlyoffice/documentserver

#----[COMMON]----#
COPY --from=build /onlyoffice/DocumentServer/server/build/server/Common/config \
    server/Common/config

COPY --from=build /onlyoffice/DocumentServer/server/build/server/Common/node_modules \
    server/Common/node_modules

COPY --from=build /onlyoffice/DocumentServer/server/build/server/Common/config/log4js \
    /etc/onlyoffice/documentserver/log4js
#----------------#

#----[FILE-CONVERTER]----#
COPY --from=build /onlyoffice/DocumentServer/server/build/server/FileConverter/converter \
    server/FileConverter/converter

COPY --from=build /onlyoffice/DocumentServer/server/build/server/FileConverter/node_modules \
    server/FileConverter/node_modules

COPY --from=build /onlyoffice/DocumentServer/server/build/server/FileConverter/bin \
    server/FileConverter/bin

COPY --from=build /onlyoffice/DocumentServer/core/build/lib/linux_64/* \
    server/FileConverter/bin/

COPY --from=build /onlyoffice/DocumentServer/core/Common/3dParty/icu/linux_64/build/*.58 \
    server/FileConverter/bin/

COPY ./DoctRenderer.config server/FileConverter/bin/
#------------------------#

#----[DOC-SERVICE]----#
COPY --from=build /onlyoffice/DocumentServer/server/build/server/DocService/docservice \
    server/DocService/docservice

COPY --from=build /onlyoffice/DocumentServer/server/build/server/DocService/node_modules \
    server/DocService/node_modules
#---------------------#

#----[SPELLCHECKER]----#
COPY --from=build /onlyoffice/DocumentServer/server/build/server/SpellChecker/spellchecker \
    server/SpellChecker/spellchecker

COPY --from=build /onlyoffice/DocumentServer/server/build/server/SpellChecker/node_modules \
    server/SpellChecker/node_modules

COPY ./dictionaries server/SpellChecker/dictionaries
#----------------------#

COPY ./run.sh ./run.sh
ENTRYPOINT ./run.sh


############[STAGE 3]###############
FROM common as development

#----[WEB-APPS]----#
COPY --from=build /onlyoffice/DocumentServer/web-apps web-apps
#------------------#

#----[SDKJS]----#
COPY --from=build /onlyoffice/DocumentServer/sdkjs sdkjs
COPY ./sdkjs-plugins sdkjs-plugins
#---------------#

#----[FONTS]----#
COPY --from=build /onlyoffice/DocumentServer/fonts fonts
COPY --from=build /onlyoffice/DocumentServer/font-files/selection/font_selection.bin server/FileConverter/bin/font_selection.bin
COPY --from=build /onlyoffice/DocumentServer/font-files/allfonts/AllFonts.js server/FileConverter/bin/AllFonts.js
COPY --from=build /onlyoffice/DocumentServer/font-files/allfonts-web/AllFonts.js sdkjs/common/AllFonts.js
#---------------#


############[STAGE 5]###############
FROM common as production

#----[WEB-APPS]----#
COPY --from=build /onlyoffice/DocumentServer/web-apps/deploy/web-apps web-apps
#------------------#

#----[SDKJS]----#
#Necessary to copy some files
COPY --from=build /onlyoffice/DocumentServer/sdkjs sdkjs
COPY --from=build /onlyoffice/DocumentServer/sdkjs/deploy/sdkjs sdkjs
COPY ./sdkjs-plugins sdkjs-plugins
#---------------#

#----[FONTS]----#
COPY --from=build /onlyoffice/DocumentServer/fonts fonts
COPY --from=build /onlyoffice/DocumentServer/font-files/selection/font_selection.bin server/FileConverter/bin/font_selection.bin
COPY --from=build /onlyoffice/DocumentServer/font-files/allfonts/AllFonts.js server/FileConverter/bin/AllFonts.js
COPY --from=build /onlyoffice/DocumentServer/font-files/allfonts-web/AllFonts.js sdkjs/common/AllFonts.js
#---------------#
