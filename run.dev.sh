#!/bin/bash

SERVER_DIR="/var/www/onlyoffice/documentserver/server"
CONFIG_DIR="/var/www/onlyoffice/documentserver/server/Common/config"
LOG_DIR="/var/log/onlyoffice"
DS_LOGS_DIR="${LOG_DIR}/documentserver"

# creating log folders
mkdir -p ${DS_LOGS_DIR}/converter
mkdir -p ${DS_LOGS_DIR}/docservice
mkdir -p ${DS_LOGS_DIR}/spellchecker
mkdir -p ${DS_LOGS_DIR}/metrics

# change log folder rights
chmod -R 755 ${LOG_DIR}

#starting database
service postgresql start

#starting message queue
service rabbitmq-server start

#starting server
service nginx start

cd ${SERVER_DIR}/FileConverter
LD_LIBRARY_PATH=./bin NODE_ENV=production-linux NODE_CONFIG_DIR=${CONFIG_DIR} ./converter &

cd ${SERVER_DIR}/SpellChecker
NODE_ENV=production-linux NODE_CONFIG_DIR=${CONFIG_DIR} ./spellchecker &

cd ${SERVER_DIR}/DocService
NODE_ENV=production-linux NODE_CONFIG_DIR=${CONFIG_DIR} ./docservice &

tail -f /var/log/nginx/access.log /var/log/nginx/error.log
