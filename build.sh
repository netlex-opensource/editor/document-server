#!/bin/bash

set -e

######
# v8 #
######
cd /onlyoffice/DocumentServer/core/Common/3dParty/v8
if [ ! -d "depot_tools" ]; then
   git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi
PATH="/onlyoffice/DocumentServer/core/Common/3dParty/v8/depot_tools/:${PATH}"
if [ ! -d "v8" ]; then
    fetch v8
fi
cd v8
git checkout branch-heads/6.0
gclient sync -D
gn gen out.gn/linux_64 --args="is_debug=false target_cpu=\"x64\" v8_target_cpu=\"x64\" v8_static_library=true is_component_build=false v8_use_snapshot=false is_clang=false use_sysroot=false"
ninja -C out.gn/linux_64

#########
# boost #
#########
cd /onlyoffice/DocumentServer/core/Common/3dParty/boost
if [ ! -d "boost_1_58_0" ]; then
    git clone --recursive --depth=1 https://github.com/boostorg/boost.git boost_1_58_0 -b boost-1.58.0
fi
cd boost_1_58_0
./bootstrap.sh --with-libraries=filesystem,system,date_time,regex
./b2 headers
./b2 --clean
./bjam --prefix=./../build/linux_64 link=static cxxflags=-fPIC install

#######
# icu #
#######
cd /onlyoffice/DocumentServer/core/Common/3dParty
if [ ! -d "icu/linux_64/build" ]; then
    wget https://github.com/unicode-org/icu/releases/download/release-58-3/icu4c-58_3-src.tgz
    tar -xf icu4c-58_3-src.tgz
    rm icu4c-58_3-src.tgz
    sed -i 's/xlocale/locale/' icu/source/i18n/digitlst.cpp
    mkdir -p icu/linux_64/build
fi
cd icu/linux_64/build
../../source/runConfigureICU Linux
make -j 7
make install
cp -r lib/* ./

#######
# cef #
#######
cd /onlyoffice/DocumentServer/core/Common/3dParty/cef
if [ ! -d "linux_64/build" ]; then
    mkdir -p linux_64/build
    cd linux_64/build
    wget http://d2ettrnqo7v976.cloudfront.net/cef/3770/linux_64/cef_binary.7z
    7z x -y cef_binary.7z -o./
    rm cef_binary.7z
    cp -r cef_binary/Release/* ./
    cp -r cef_binary/Resources/* ./
fi

########
# core #
########
cd /onlyoffice/DocumentServer/core
make -j 7

############
# web-apps #
############
cd /onlyoffice/DocumentServer/web-apps/build
npm install
grunt
cd sprites
npm install
grunt

#########
# sdkjs #
#########
cd /onlyoffice/DocumentServer/sdkjs/build
npm install
grunt develop
grunt

##########
# server #
##########
cd /onlyoffice/DocumentServer/server
npm install
grunt --no-color -v

#################
# server/common #
#################
cd /onlyoffice/DocumentServer/server/Common
npm install

###################
# server binaries #
###################
cd /onlyoffice/DocumentServer/server/DocService
npm install
pkg . -t node10-linux --options max_old_space_size=4096 -o docservice
cd ../FileConverter
mkdir -p bin
cp /onlyoffice/DocumentServer/core/build/bin/linux_64/x2t ./bin
npm install
pkg . -t node10-linux -o converter
cd ../Metrics
npm install
pkg . -t node10-linux -o metrics
cd ../SpellChecker
npm install
pkg . -t node10-linux -o spellchecker

##############
# make fonts #
##############
cd /onlyoffice/DocumentServer/core/build/bin/linux_64
rm -rf /onlyoffice/DocumentServer/core-fonts/fonts-beng-extra
cp /onlyoffice/DocumentServer/core/Common/3dParty/icu/linux_64/build/*.58 /onlyoffice/DocumentServer/core/build/lib/linux_64
mkdir -p /onlyoffice/DocumentServer/fonts
LD_LIBRARY_PATH=/onlyoffice/DocumentServer/core/build/lib/linux_64 ./allfontsgen \
--input=/onlyoffice/DocumentServer/core-fonts \
--allfonts-web=/onlyoffice/DocumentServer/sdkjs/common/AllFonts.js \
--allfonts=./AllFonts.js \
--images=/onlyoffice/DocumentServer/sdkjs/common/Images \
--selection=./font_selection.bin \
--use-system=true \
--output-web=/onlyoffice/DocumentServer/fonts

cp /onlyoffice/DocumentServer/DoctRenderer.config /onlyoffice/DocumentServer/server/FileConverter/bin/
