#!/bin/bash

service postgresql start 
service rabbitmq-server start 
service nginx start

cd /var/www/onlyoffice/documentserver/server/FileConverter
LD_LIBRARY_PATH=./bin NODE_ENV=production-linux NODE_CONFIG_DIR=../Common/config ./converter &

cd ../SpellChecker
NODE_ENV=production-linux NODE_CONFIG_DIR=../Common/config ./spellchecker &

cd ../DocService
NODE_ENV=production-linux NODE_CONFIG_DIR=../Common/config ./docservice &

tail -f /var/log/nginx/access.log /var/log/nginx/error.log
